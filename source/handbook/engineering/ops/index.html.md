---
layout: markdown_page
title: "Ops Department"
---

## On this page
{:.no_toc}

- TOC
{:toc}

[Dalia Havens](/company/team/#dhavens) is the Director for the Ops Department. Feel free to check out her [README](/handbook/engineering/ops/director/).
## Teams

There are a number of teams within the Ops Backend group:

* [Verify](/handbook/engineering/ops/verify/)
* [Release](/handbook/engineering/ops/release/)
* [Configure](/handbook/engineering/ops/configure/)
* [Monitor](/handbook/engineering/development/ops/monitor/)
* [Secure](/handbook/engineering/ops/secure/)
* [Serverless](/handbook/engineering/ops/serverless/)

Each team has a different focus on what issues to work on for each
release. The following information is not meant to be a set of hard-and-fast
rules, but as a guideline as to what team decides can best improve certain
areas of GitLab.

APIs should be shared responsibility between all teams within the
Backend group.

There is a backend group call every Tuesday, before the company call. You should
have been invited when you joined; if not, ask your team lead!

[Find the product manager mapping to engineering teams in the product handbook](/handbook/product)
